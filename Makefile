include Makefile.config

PROJECTNAME = hungry_monsters

target = $(PROJECTNAME).lnx

assets =  $(BOBCAT)/empty_sprite.o\
	  	src/gfx/hungry.o\
		src/gfx/press_button.o\
		src/gfx/monster_head.o\
		src/gfx/logo_sm.o\
		src/gfx/hud_path.o\
	  	src/gfx/emote_exclam_01.o\
	  	src/gfx/emote_question_01.o\
	  	src/gfx/player_afraid_01.o\
	  	src/gfx/player_afraid_02.o\
	  	src/gfx/player_run_01.o\
	  	src/gfx/player_run_02.o\
	  	src/gfx/player_run_03.o\
	  	src/gfx/player_run_04.o\
	  	src/gfx/player_run_05.o\
	  	src/gfx/player_run_06.o\
	  	src/gfx/player_jump_01.o\
	  	src/gfx/player_jump_02.o\
	  	src/gfx/player_jump_03.o\
	  	src/gfx/player_jump_04.o\
	  	src/gfx/player_jump_05.o\
	  	src/gfx/player_jump_06.o\
	  	src/gfx/level1_ceil.o\
	  	src/gfx/level1_floor.o\
	  	src/gfx/level1_bones.o\
	  	src/gfx/level1_chain.o\
	  	src/gfx/rock_01.o\
	  	src/gfx/dragon_01.o\
	  	src/gfx/dragon_02.o\
	  	src/gfx/warrior_01.o\
	  	src/gfx/warrior_02.o\
	  	src/gfx/warrior_03.o\
	  	src/gfx/warrior_04.o\
	  	src/gfx/warrior_05.o\
	  	src/gfx/warrior_06.o\
	  	src/gfx/lance_01.o\
	  	src/gfx/bat_idle_01.o\
	  	src/gfx/bat_idle_02.o\
	  	src/gfx/bat_idle_03.o\
	  	src/gfx/bat_attack_01.o\
	  	src/gfx/bat_attack_02.o\
	  	src/gfx/level2_side.o\
	  	src/gfx/level2_bg.o\
	  	src/gfx/level2_bridge.o\
	  	src/gfx/level2_exit.o\
	  	src/gfx/spider_01.o\
	  	src/gfx/spider_02.o\
	  	src/gfx/spider_04.o\
	  	src/gfx/spider_05.o\
	  	src/gfx/web_01.o\
	  	src/gfx/web_02.o\
	  	src/gfx/web_03.o\
	  	src/gfx/web_04.o\
	  	src/gfx/web_05.o\
	  	src/gfx/web_06.o\
	  	src/gfx/whip.o\
	  	src/gfx/chest_01.o\
	  	src/gfx/chest_02.o\
	  	src/gfx/chest_03.o\
	  	src/gfx/chest_04.o\
	  	src/gfx/chest_05.o\
	  	src/sound/song.o\

pals = src/gfx/title_screen.pal\
	   src/gfx/game_sheet.pal\
	  src/gfx/logo_sm.pal

objects = 	src/directory.o\
			src/logo.o\
			src/title.o\
			src/player.o\
			src/game_level4.o\
			src/game_level3.o\
			src/game_level2.o\
			src/game_level1.o\
			src/game.o\
			src/gameover.o\
			src/hungry_monsters.o\
			$(BOBCAT)/bobcat.o

all: $(target) 

#asset first, else it will overwrite or delete pal
#objects last, so I could export pal first
$(target) : $(assets) $(pals) $(objects)
	$(CL) -t $(SYS) -m $(PROJECTNAME).map -C $(PROJECTNAME).cfg -o $@ $(assets) $(objects) 

clean:
	-$(RM) $(target) $(objects) $(assets) $(pals)
	-$(RM) src/gfx/*.pal
	-$(RM) src/gfx/*.spr
	-$(RM) src/gfx/*.s
	-$(RM) $(PROJECTNAME).map

###############
##### Logo sprites
src/gfx/logo_sm.o: ASSETS_SEGMENT=LOGO_RODATA

###############
##### Title sprites
src/gfx/hungry.o: ASSETS_SEGMENT=TITLE_RODATA
src/gfx/hungry.spr:src/gfx/title_screen.bmp
	$(SPRPCK) -s4 -p2 -t6 -o080008 -S077030 -r001001 $< $(basename $@)

src/gfx/press_button.o: ASSETS_SEGMENT=TITLE_RODATA
src/gfx/press_button.spr:src/gfx/title_screen.bmp
	$(SPRPCK) -s4 -p2 -t6 -o022084 -S123007 -r001001 $< $(basename $@)

src/gfx/monster_head.o: ASSETS_SEGMENT=TITLE_RODATA
src/gfx/monster_head.spr:src/gfx/title_screen.bmp
	$(SPRPCK) -s4 -p2 -t6 -o000000 -S090080 -r001001 $< $(basename $@)



###############
##### Game sprites common on all parts
src/gfx/hud_path.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o128196 -S001002 -r001001 $< $(basename $@)

src/gfx/emote_exclam_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o188001 -S014015 -a005015 -r001001 $< $(basename $@)
src/gfx/emote_question_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o188017 -S014015 -a005015 -r001001 $< $(basename $@)

src/gfx/player_afraid_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o120004 -S022028 -a012028 -r001001 $< $(basename $@)
src/gfx/player_afraid_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o151004 -S024028 -a013028 -r001001 $< $(basename $@)
	
src/gfx/player_run_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o118064 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_run_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o150064 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_run_03.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o182064 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_run_04.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o214064 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_run_05.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o246064 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_run_06.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o278064 -S032032 -a016032 -r001001 $< $(basename $@)

src/gfx/player_jump_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o118096 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_jump_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o150096 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_jump_03.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o182096 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_jump_04.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o214096 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_jump_05.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o246096 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/player_jump_06.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o278096 -S032032 -a016032 -r001001 $< $(basename $@)



## LEVEL1 and 3 so linked in common
src/gfx/level1_ceil.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o000310 -S160032 -r001001 $< $(basename $@)
src/gfx/level1_floor.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o000344 -S160038 -r001001 $< $(basename $@)

## LEVEL1
src/gfx/level1_chain.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/level1_chain.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o162310 -S007064 -r001001 $< $(basename $@)
src/gfx/level1_bones.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/level1_bones.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o173310 -S032024 -r001001 $< $(basename $@)


src/gfx/rock_01.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/rock_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o308056 -S012008 -r001001 $< $(basename $@)
src/gfx/dragon_01.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/dragon_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o045160 -S067075 -r001001 $< $(basename $@)
	
src/gfx/dragon_02.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/dragon_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o163166 -S066056 -r001001 $< $(basename $@)

	
src/gfx/warrior_01.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/warrior_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o118128 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/warrior_02.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/warrior_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o150128 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/warrior_03.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/warrior_03.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o182128 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/warrior_04.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/warrior_04.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o214128 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/warrior_05.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/warrior_05.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o246128 -S032032 -a016032 -r001001 $< $(basename $@)
src/gfx/warrior_06.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/warrior_06.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o278128 -S032032 -a016032 -r001001 $< $(basename $@)

src/gfx/lance_01.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/lance_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o232023 -S028005 -a014003 -r001001 $< $(basename $@)

src/gfx/bat_idle_01.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/bat_idle_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o233002 -S024014 -a011009 -r001001 $< $(basename $@)
src/gfx/bat_idle_02.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/bat_idle_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o265001 -S024014 -a011010 -r001001 $< $(basename $@)
src/gfx/bat_idle_03.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/bat_idle_03.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o297000 -S023014 -a011011 -r001001 $< $(basename $@)

src/gfx/bat_attack_01.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/bat_attack_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o268018 -S020012 -a007008 -r001001 $< $(basename $@)
src/gfx/bat_attack_02.o: ASSETS_SEGMENT=LEVEL1_RODATA
src/gfx/bat_attack_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o300018 -S020012 -a008008 -r001001 $< $(basename $@)

## LEVEL1 and 2 so linked in common
src/gfx/level2_side.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o000386 -S011102 -r001001 $< $(basename $@)
src/gfx/level2_bg.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o028386 -S023102 -r001001 $< $(basename $@)

src/gfx/level2_bridge.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o173342 -S032004 -r001001 $< $(basename $@)

## LEVEL2
src/gfx/level2_exit.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/level2_exit.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o028386 -S023050 -r001001 $< $(basename $@)

src/gfx/spider_01.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/spider_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o000262 -S041039 -a021003 -r001001 $< $(basename $@)
src/gfx/spider_02.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/spider_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o044262 -S041042 -a022003 -r001001 $< $(basename $@)
src/gfx/spider_04.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/spider_04.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o139262 -S041039 -a020003 -r001001 $< $(basename $@)
src/gfx/spider_05.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/spider_05.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o185262 -S041042 -a020003 -r001001 $< $(basename $@)

	
src/gfx/web_01.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/web_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o146248 -S004002 -a002001 -r001001 $< $(basename $@)
src/gfx/web_02.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/web_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o178247 -S004004 -a002002 -r001001 $< $(basename $@)
src/gfx/web_03.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/web_03.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o208244 -S007008 -a003004 -r001001 $< $(basename $@)
src/gfx/web_04.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/web_04.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o235240 -S015014 -a007007 -r001001 $< $(basename $@)
src/gfx/web_05.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/web_05.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o263237 -S023020 -a012010 -r001001 $< $(basename $@)
src/gfx/web_06.o: ASSETS_SEGMENT=LEVEL2_RODATA
src/gfx/web_06.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o297238 -S019018 -a010009 -r001001 $< $(basename $@)

## LEVEL3
src/gfx/whip.o: ASSETS_SEGMENT=LEVEL3_RODATA
src/gfx/whip.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o227334 -S016016 -a008016 -r001001 $< $(basename $@)
	
src/gfx/chest_01.o: ASSETS_SEGMENT=LEVEL3_RODATA
src/gfx/chest_01.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o178366 -S017013 -a009013 -r001001 $< $(basename $@)

src/gfx/chest_02.o: ASSETS_SEGMENT=LEVEL3_RODATA
src/gfx/chest_02.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o202368 -S019011 -a010011 -r001001 $< $(basename $@)

src/gfx/chest_03.o: ASSETS_SEGMENT=LEVEL3_RODATA
src/gfx/chest_03.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o229364 -S015015 -a008015 -r001001 $< $(basename $@)

src/gfx/chest_04.o: ASSETS_SEGMENT=LEVEL3_RODATA
src/gfx/chest_04.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o253365 -S017014 -a009014 -r001001 $< $(basename $@)

src/gfx/chest_05.o: ASSETS_SEGMENT=LEVEL3_RODATA
src/gfx/chest_05.spr:src/gfx/game_sheet.bmp
	$(SPRPCK) -s4 -p2 -t6 -o278358 -S017021 -a009021 -r001001 $< $(basename $@)

.PRECIOUS: %.spr %.pal