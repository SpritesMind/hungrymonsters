SYMBOLS {
    #note : weak value could be set by arg to linker -> -D valuename=value

    #based on https://atarilynxdeveloper.wordpress.com/2013/12/30/programming-tutorial-part-15memory-and-segments/
    
    #useful values
    __LNXHEADERSIZE__:      type = weak, value = 64;
    __BLOCKSIZE__:          type = weak, value = $0400; # needed by loader related methods (Karri used his own values)
    __BANK0BLOCKSIZE__:     type = weak, value = __BLOCKSIZE__; # bank 0 cart block size (1024) (see cc65 exehdr.s)
    __BANK1BLOCKSIZE__:     type = weak, value = $0000; # bank 1 block size (see cc65 exehdr.s)
    __BOOTLDR_SIZE__:       type = export, value = $cb; #$CB is CC65 loader size
    __FILECOUNT__:          type = weak, value = 8;
    __DIRECTORYSIZE__:      type = export, value = __FILECOUNT__*8; # 8bytes per entry (Common, level1, level2, level3, level4, ...)

    #RAM (FFFF to 0000)
    __IVT__:                type = export, value = $FFF8;   #addresse of vector table
    __SCREENBUFFER_SIZE__:  type = export, value = 160*102/2;    #4bits per pixel   
    __STACKSIZE__:          type = weak, value = $0800; # 2k stack
    __SCREENBUFFER1__:      type = export, value = __IVT__ - __SCREENBUFFER_SIZE__; 
    __SCREENBUFFER2__:      type = export, value = __SCREENBUFFER1__ - __SCREENBUFFER_SIZE__;
    __RAM_END__:            type = export, value = __SCREENBUFFER2__ - __STACKSIZE__;
    __RAM_START__:          type = export, value = $200;
    
    __SHARED_RAM_SIZE__:    type = weak, value = $3000; # adjust according .map
    __SHARED_RAM__:         type = export, value = __RAM_START__;
    __COMMON_RAM__:         type = export, value = __SHARED_RAM__ + __SHARED_RAM_SIZE__;  #where we put common code and data
    __COMMON_RAM_SIZE__:    type = export, value = __RAM_END__ - __COMMON_RAM__;
    
    
    #ROM (0000 to ....)
    __STARTOFDIRECTORY__:   type = weak, value = 0 + __BOOTLDR_SIZE__; # start just after loader 
#this will link default Game/Manufacturer header (see cc65/exehdr.s)
#   __EXEHDR__:             type = import;
#this will link default bootlooader (see cc65/bootldr.s)
    __BOOTLDR__:            type = import;
#this will inlude one unique file default configuration (see cc65/defdir.s) and loader related method
#warning : if you don't use it, game won't boot
#warning : if you don't use it but include your own directory configuration, cc65 won't link loader related method (load, seek...)
#          so you could use this import or export it from your own configuration
#           .export         __DEFDIR__: absolute = 1
#    __DEFDIR__:             type = import; 

}

#linker will create the binary file that holds all code and data in the order that the memory areas have been defined in the MEMORY section.
#The linker will emit the binary image with “files” for each of the areas that have a file=%O in their definition.

MEMORY {
    ZP:     file = "", define = yes, start = $0000, size = $0100;
    HEADER: file = %O,               start = $0000, size = __LNXHEADERSIZE__, fill = yes, fillval = $00; #fill is mandatory when using your own because of the 3 last char
    BOOT:   file = %O,               start = $0200, size = __STARTOFDIRECTORY__;
    DIR:    file = %O,               start = $0000, size = __DIRECTORYSIZE__;
    LOGO:   file = %O, define = yes, start = $0200, size = __SHARED_RAM_SIZE__;
    TITLE:  file = %O, define = yes, start = $0200, size = __SHARED_RAM_SIZE__;
    LEVEL1: file = %O, define = yes, start = $0200, size = __SHARED_RAM_SIZE__;
    LEVEL2: file = %O, define = yes, start = $0200, size = __SHARED_RAM_SIZE__;
    LEVEL3: file = %O, define = yes, start = $0200, size = __SHARED_RAM_SIZE__;
    LEVEL4: file = %O, define = yes, start = $0200, size = __SHARED_RAM_SIZE__;
    GAMEOVER: file = %O, define = yes, start = $0200, size = __SHARED_RAM_SIZE__;
    MAIN:   file = %O, define = yes, start = $0200 + __SHARED_RAM_SIZE__,  size = __COMMON_RAM_SIZE__;
}

#define=yes will expose XXX_SIZE__ (size of segment) and XXX_LOAD__ (address where i should be loaded ..see memory)
SEGMENTS {
    ZEROPAGE:  load = ZP,     type = zp;
    EXTZP:     load = ZP,     type = zp,                optional = yes;
    APPZP:     load = ZP,     type = zp,                optional = yes;
    
#beware to order!
    EXEHDR:    load = HEADER, type = ro;
    BOOTLDR:   load = BOOT,   type = ro;
    DIRECTORY: load = DIR,    type = ro;

    STARTUP:   load = MAIN,   type = ro,  define = yes;
    LOWCODE:   load = MAIN,   type = ro,  define = yes, optional = yes;
    ONCE:      load = MAIN,   type = ro,  define = yes, optional = yes;
    CODE:      load = MAIN,   type = ro,  define = yes;
    RODATA:    load = MAIN,   type = ro,  define = yes;
    DATA:      load = MAIN,   type = rw,  define = yes;
    BSS:       load = MAIN,   type = bss, define = yes;

    LOGO_CODE:  load = LOGO, type = ro, define = yes;
    LOGO_RODATA:load = LOGO, type = ro, define = yes;
    LOGO_DATA:  load = LOGO, type = rw, define = yes;
    LOGO_BSS:   load = LOGO, type = bss, optional = yes;

    TITLE_CODE:  load = TITLE, type = ro, define = yes;
    TITLE_RODATA:load = TITLE, type = ro, define = yes;
    TITLE_DATA:  load = TITLE, type = rw, define = yes;
    TITLE_BSS:   load = TITLE, type = bss, optional = yes;

    LEVEL1_CODE:  load = LEVEL1, type = ro, define = yes;
    LEVEL1_RODATA:load = LEVEL1, type = ro, define = yes;
    LEVEL1_DATA:  load = LEVEL1, type = rw, define = yes;
    LEVEL1_BSS:   load = LEVEL1, type = bss, optional = yes;

    LEVEL2_CODE:  load = LEVEL2, type = ro, define = yes;
    LEVEL2_RODATA:load = LEVEL2, type = ro, define = yes;
    LEVEL2_DATA:  load = LEVEL2, type = rw, define = yes;
    LEVEL2_BSS:   load = LEVEL2, type = bss, optional = yes;

    LEVEL3_CODE:  load = LEVEL3, type = ro, define = yes;
    LEVEL3_RODATA:load = LEVEL3, type = ro, define = yes;
    LEVEL3_DATA:  load = LEVEL3, type = rw, define = yes;
    LEVEL3_BSS:   load = LEVEL3, type = bss, optional = yes;

    LEVEL4_CODE:  load = LEVEL4, type = ro, define = yes;
    LEVEL4_RODATA:load = LEVEL4, type = ro, define = yes;
    LEVEL4_DATA:  load = LEVEL4, type = rw, define = yes;
    LEVEL4_BSS:   load = LEVEL4, type = bss, optional = yes;

    GAMEOVER_CODE:  load = GAMEOVER, type = ro, define = yes;
    GAMEOVER_RODATA:load = GAMEOVER, type = ro, define = yes;
    GAMEOVER_DATA:  load = GAMEOVER, type = rw, define = yes;
    GAMEOVER_BSS:   load = GAMEOVER, type = bss, optional = yes;
}

FEATURES {
    CONDES: type    = constructor,
            label   = __CONSTRUCTOR_TABLE__,
            count   = __CONSTRUCTOR_COUNT__,
            segment = ONCE;
    CONDES: type    = destructor,
            label   = __DESTRUCTOR_TABLE__,
            count   = __DESTRUCTOR_COUNT__,
            segment = RODATA;
    CONDES: type    = interruptor,
            label   = __INTERRUPTOR_TABLE__,
            count   = __INTERRUPTOR_COUNT__,
            segment = RODATA,
            import  = __CALLIRQ__;
}
