#include "hungry_monsters.h"


#define PLAYER_BASE_Y   (63+32)
#define PLAYER_BB_X         (60-5)
#define PLAYER_BB_Y         (PLAYER_BASE_Y - 24) //NOT WHILE JUMPING !!
#define PLAYER_BB_WIDTH     (10)
#define PLAYER_BB_HEIGHT    (20)



extern unsigned char player_afraid_01[]; 
extern unsigned char player_afraid_02[]; 
extern unsigned char player_run_01[]; 
extern unsigned char player_run_02[]; 
extern unsigned char player_run_03[]; 
extern unsigned char player_run_04[]; 
extern unsigned char player_run_05[];
extern unsigned char player_run_06[];  
extern unsigned char player_jump_01[]; 
extern unsigned char player_jump_02[]; 
extern unsigned char player_jump_03[]; 
extern unsigned char player_jump_04[]; 
extern unsigned char player_jump_05[]; 
extern unsigned char player_jump_06[]; 




#define JUMP_ACC    14

unsigned char player_animSpeed = 0;
int jumpAcc;



void player_swichState(int newState)
{
    if (newState == player_properties.state)   return;
    
    player_properties.animCounter = 0;
    player_properties.frame = 0;
    player_properties.state = newState; 

    bobcat_unflipH(player);
    player.vpos = PLAYER_BASE_Y;

    switch(newState){
        case STATE_AFRAID:
            player.data = player_afraid_01;
            player_animSpeed = 5;
            break;
            
        case STATE_RUN:
            player.data = player_run_01;
            player_animSpeed = 3;
            break;

        case STATE_JUMP:
            player.data = player_jump_01;
            player_animSpeed = 7;
            jumpAcc = -JUMP_ACC;
            break;

        case STATE_FALL:
            player.data = player_afraid_01;
            player_animSpeed = 5;
            break;

    }
}


void player_init(){
    bobcat_enableSprite(player);
    bobcat_unflipH(player);
    player.hpos = -20;
    player.vpos = PLAYER_BASE_Y;
    player.data = player_afraid_01;

    player_properties.state  = -1;
}
void player_update(){
    player_properties.animCounter++;

    if (player_properties.state == STATE_JUMP)  
    {
        if (player_properties.animCounter & 1)
        {
            jumpAcc++;
            player.vpos += (jumpAcc>>2);
            
            if (jumpAcc == JUMP_ACC)  
            {
                player_swichState(STATE_RUN);
            }
        }

    }  
    
    if (player_properties.animCounter == player_animSpeed){
            player_properties.frame++;
            if (player_properties.state == STATE_AFRAID){
                player_properties.frame &= 0x01; //2 frames
                player.data = (player_properties.frame&1)?player_afraid_01:player_afraid_02;
            }
            if (player_properties.state == STATE_RUN){
                player_properties.frame %= 6; //6 frames
                switch (player_properties.frame)
                {
                    case 1:
                        player.data = player_run_02;
                        break;
                    case 2:
                        player.data = player_run_03;
                        break;
                    case 3:
                        player.data = player_run_04;
                        break;
                    case 4:
                        player.data = player_run_05;
                        break;
                    case 5:
                        player.data = player_run_06;
                        break;
                    
                    default:
                        player.data = player_run_01;
                        break;
                }
            }
            if (player_properties.state == STATE_JUMP){
                
                switch (player_properties.frame)
                {
                    case 0:
                        player.data = player_run_01;
                        break;
                    case 1:
                        player.data = player_jump_02;
                        break;
                    case 2:
                        player.data = player_jump_03;
                        break;
                    case 3:
                        player.data = player_jump_04;
                        break;
                    case 4:
                        player.data = player_jump_05;
                        break;
                    case 5:
                    default:
                        player.data = player_jump_06;
                        break;
                }
            }

            if (player_properties.state == STATE_FALL){
                //TODO
            }
           
            player_properties.animCounter = 0;
    }
}