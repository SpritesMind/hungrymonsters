#include "hungry_monsters.h"


#pragma bss-name    ("LEVEL2_BSS")
#pragma code-name   ("LEVEL2_CODE")
#pragma data-name   ("LEVEL2_DATA")
#pragma rodata-name ("LEVEL2_RODATA")


extern char src_gfx_game_sheet_pal[]; //see game_level1

extern unsigned char level2_side[]; 
extern unsigned char level2_bg[]; 


////////////////////////
/// EXIT
#define exit shoot

extern unsigned char level2_exit[]; 


////////////////////////
/// BRIDGE
#define bridge_01 foreground_01
#define bridge_02 foreground_02
#define bridge_03 foreground_03
#define bridge_04 foreground_04
#define bridge_05 shoot

extern unsigned char level2_bridge[]; 



////////////////////////
/// SPIDER

#define SPIDER_STATE_ENTRY  0
#define SPIDER_STATE_IDLE   1
#define SPIDER_STATE_CHASE  2


#define SPIDER_SPEED    20

#define spider  monster
#define spider_propertiers  monster_properties
#define rope shoot


extern unsigned char spider_01[]; 
extern unsigned char spider_02[]; 
//extern unsigned char spider_03[];  //same as 01
extern unsigned char spider_04[]; 
extern unsigned char spider_05[]; 
//extern unsigned char spider_06[];  //same as 04

void spider_init(){
    bobcat_enableSprite(spider);
    spider.vpos = -100;
    spider.data = spider_01;
    spider.hpos = SCREEN_WIDTH/2;

    spider_propertiers.animCounter  = 0;
    spider_propertiers.frame  = 0;
    spider_propertiers.state  = NO_STATE;
}

void spider_update(){
    spider_propertiers.animCounter++;
    if (spider_propertiers.animCounter == SPIDER_SPEED)  spider.data = spider_02;
    if (spider_propertiers.animCounter == SPIDER_SPEED*2) spider.data = spider_01;
    if (spider_propertiers.animCounter == SPIDER_SPEED*3) spider.data =  spider_04;
    if (spider_propertiers.animCounter == SPIDER_SPEED*4)  spider.data = spider_05;
    if (spider_propertiers.animCounter == SPIDER_SPEED*5) spider.data = spider_04;
    if (spider_propertiers.animCounter == SPIDER_SPEED*6) 
    {
        spider.data = spider_01;
        spider_propertiers.animCounter = 0;
    }  
}



////////////
/// WEBs
#define DEPLOY_START_TIME   20

#define WEB_STATE_IDLE      0
#define WEB_STATE_CAPTURE   1

#define web1  enemy1
#define web1_properties  enemy1_properties
#define web2  enemy2
#define web2_properties  enemy2_properties
#define web3  enemy3
#define web3_properties  enemy3_properties


extern unsigned char web_01[]; 
extern unsigned char web_02[]; 
extern unsigned char web_03[]; 
extern unsigned char web_04[]; 
extern unsigned char web_05[]; 
extern unsigned char web_06[]; 

void webs_init(){
    bobcat_enableSprite(web1);
    web1.hpos = HIDDEN_X;
    web1.data = web_01;
    web1_properties.animCounter  = 0;
    web1_properties.frame  = 0;
    web1_properties.state  = NO_STATE;
    
    bobcat_enableSprite(web2);
    web2.hpos = HIDDEN_X;
    web2.data = web_01;
    web2_properties.animCounter  = 0;
    web2_properties.frame  = 0;
    web2_properties.state  = NO_STATE;

    bobcat_enableSprite(web3);
    web3.hpos = HIDDEN_X;
    web3.data = web_01;
    web3_properties.animCounter  = 0;
    web3_properties.frame  = 0;
    web3_properties.state  = NO_STATE;
}


void webs_launch(){
    int decal = spider.hpos;
    if (spider.hpos > player.hpos)  decal -= 8;
    if (spider.hpos < player.hpos)  decal += 8;

    if (web1.hpos == HIDDEN_X)
    {
        bobcat_enableSprite(web1);
        web1.data = web_01;
        web1.vpos = 10;
        web1.hpos = decal;

        web1_properties.animCounter  = 0;
        web1_properties.frame  = 0;
        return;
    }

    
    if (web2.hpos == HIDDEN_X)
    {
        bobcat_enableSprite(web2);
        web2.data = web_01;
        web2.vpos = 10;
        web2.hpos = decal;

        web2_properties.animCounter  = 0;
        web2_properties.frame  = 0;
        return;
    }

    
    if (web3.hpos == HIDDEN_X)
    {
        bobcat_enableSprite(web3);
        web3.data = web_01;
        web3.vpos = 10;
        web3.hpos = decal;

        web3_properties.animCounter  = 0;
        web3_properties.frame  = 0;
        return;
    }
}

void webs_update(){
    if (web1.hpos != HIDDEN_X){
        web1_properties.animCounter++;

        if (web1_properties.state == WEB_STATE_CAPTURE){
            web1.hpos = player.hpos;
            web1.vpos = player.vpos - 12;
        }
        else {
            if (web1_properties.animCounter == DEPLOY_START_TIME)   web1.data = web_02;
            if (web1_properties.animCounter == DEPLOY_START_TIME+5)   web1.data = web_03;
            if (web1_properties.animCounter == DEPLOY_START_TIME+10)   web1.data = web_04;
            if (web1_properties.animCounter == DEPLOY_START_TIME+15)   web1.data = web_05;
            if ( (web1_properties.animCounter == DEPLOY_START_TIME+20) && (rand()&1))   web1.data = web_06;

            web1.vpos++;
            if (web1.vpos == (SCREEN_HEIGHT + 20)) {
                bobcat_disableSprite(web1);
                web1.hpos = HIDDEN_X;
            }
        }
    }

    if (web2.hpos != HIDDEN_X){
        web2_properties.animCounter++;
        
        if (web2_properties.state == WEB_STATE_CAPTURE){
            web2.hpos = player.hpos;
            web2.vpos = player.vpos - 12;
        }
        else {
            if (web2_properties.animCounter == DEPLOY_START_TIME)   web2.data = web_02;
            if (web2_properties.animCounter == DEPLOY_START_TIME+5)   web2.data = web_03;
            if (web2_properties.animCounter == DEPLOY_START_TIME+10)   web2.data = web_04;
            if (web2_properties.animCounter == DEPLOY_START_TIME+15)   web2.data = web_05;
            if ( (web2_properties.animCounter == DEPLOY_START_TIME+20) && (rand()&1))   web2.data = web_06;

            web2.vpos++;
            if (web2.vpos == (SCREEN_HEIGHT + 20)) {
                bobcat_disableSprite(web2);
                web2.hpos = HIDDEN_X;
            }   
        }
    }

    
    if (web3.hpos != HIDDEN_X){
        web3_properties.animCounter++;

        if (web2_properties.state == WEB_STATE_CAPTURE){
            web2.hpos = player.hpos;
            web2.vpos = player.vpos - 12;
        }
        else {
            if (web3_properties.animCounter == DEPLOY_START_TIME)   web3.data = web_02;
            if (web3_properties.animCounter == DEPLOY_START_TIME+5)   web3.data = web_03;
            if (web3_properties.animCounter == DEPLOY_START_TIME+10)   web3.data = web_04;
            if (web3_properties.animCounter == DEPLOY_START_TIME+15)   web3.data = web_05;
            if ( (web3_properties.animCounter == DEPLOY_START_TIME+20) && (rand()&1))   web3.data = web_06;

            web3.vpos++;
            if (web3.vpos == (SCREEN_HEIGHT + 20)) {
                bobcat_disableSprite(web3);
                web3.hpos = HIDDEN_X;
            }
        }
    }
}


///////////
unsigned char parallaxFX;

#define BBOX_PRECISION  4
//extern unsigned char hud_path[];
unsigned char level2_checkCollide(){
    unsigned int top, bottom, left, right;

    //web is -S019018 -a010009 so middle
    //player anchor is -a012028 for -S022028 so  middle-right
    top = player.vpos - (28 - BBOX_PRECISION) - ((18/2) - BBOX_PRECISION);
    bottom = player.vpos + (0 - BBOX_PRECISION) + ((18/2) - BBOX_PRECISION);
    left = player.hpos - (12 - BBOX_PRECISION) - ((19/2) - BBOX_PRECISION);
    right = player.hpos + (10 - BBOX_PRECISION) + ((19/2) - BBOX_PRECISION);

    if ( bobcat_isFlippedH(player) )
    {
        left+=2;
        right+=2;
    }
/*
    bobcat_enableSprite(foreground_01);
    foreground_01.data = hud_path;
    foreground_01.hpos = left;
    foreground_01.vpos = top;
    foreground_01.hsize = ( right-left ) << 8;
    foreground_01.vsize = (( bottom-top ) >>1) << 8;
*/
    if ( (web1.vpos > top) && (web1.vpos < bottom) && 
         (web1.hpos > left) && (web1.hpos < right) )
    {
        //foreground_01.vpos = SCREEN_HEIGHT;
        web1_properties.state = WEB_STATE_CAPTURE;
        web1.data = web_06;
        return 1;
    }

    if ( (web2.vpos > top) && (web2.vpos < bottom) && 
         (web2.hpos > left) && (web2.hpos < right) )
    {
        //foreground_01.vpos = SCREEN_HEIGHT;
        web2_properties.state = WEB_STATE_CAPTURE;
        web2.data = web_06;
        return 2;
    }
    
    if ( (web3.vpos > top) && (web3.vpos < bottom) && 
         (web3.hpos > left) && (web3.hpos < right) )
    {
        //foreground_01.vpos = SCREEN_HEIGHT;
        web3_properties.state = WEB_STATE_CAPTURE;
        web3.data = web_06;
        return 3;
    }

    return 0;
}


void level2_update(){
    if ( (parallaxFX == 0) || (timer & 1) ){
        background_01.vpos--;
        if (background_01.vpos == -SCREEN_HEIGHT) background_01.vpos = SCREEN_HEIGHT;
        background_02.vpos = background_01.vpos;

        background_03.vpos--;
        if (background_03.vpos == -SCREEN_HEIGHT) background_03.vpos = SCREEN_HEIGHT;
        background_04.vpos = background_03.vpos;
    }

    foreground_01.vpos--;
    if (foreground_01.vpos == -SCREEN_HEIGHT) {
        foreground_01.vpos = SCREEN_HEIGHT;
        parallaxFX = 1;
    }
    foreground_02.vpos = foreground_01.vpos;

    foreground_03.vpos--;
    if (foreground_03.vpos == -SCREEN_HEIGHT) foreground_03.vpos = SCREEN_HEIGHT;
    foreground_04.vpos = foreground_03.vpos;
}
void level2_death( ){
    timer = 0;
    bobcat_enableSprite(rope);
    rope.data = web_01;
    rope.vpos =  0;
    rope.hsize = 0x100 / 3;
    
    player_swichState(STATE_AFRAID);
    while(1){
        bobcat_clearScreen();

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        level2_update(); //we still scroll

        timer++;
        if (timer & 4)
        {
            if (spider.hpos > player.hpos)
                spider.hpos--;
            else if ( spider.hpos < player.hpos)
                spider.hpos++;
            else if (spider.vpos < (player.vpos - 40)) 
              spider.vpos++;
            else
                break;
        }

        if (timer & 0x20)
            bobcat_flipH(player);
        else
            bobcat_unflipH(player);

        rope.hpos = spider.hpos-1;
        rope.vsize = spider.vpos << 8;

        spider_update(); 
        webs_update();
        player_update(); 
    }



    while(1){
        bobcat_clearScreen();

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        level2_update(); //we still scroll

        timer++;
        if (timer & 4)
        {
            if (spider.vpos > -60)
            {
              spider.vpos--;
              player.vpos--;

              if (spider.vpos==0)   bobcat_disableSprite(rope);
            }
            else
                break;

        }
        if (timer & 0x10)
            bobcat_flipH(player);
        else
            bobcat_unflipH(player);

        rope.hpos = spider.hpos-1;
        rope.vsize = spider.vpos << 8;

        spider_update(); 
        webs_update(); //so it follows player pos
        player_update(); 
    }

}


void level2_init(){
    bobcat_unflipH(background_01);
    background_01.data = level2_side;
    background_01.hpos = 0;
    background_01.vpos = PLAYER_BASE_Y;
    
    bobcat_enableSprite(background_02);
    bobcat_flipH(background_02);
    background_02.data = level2_side;
    background_02.hpos = SCREEN_WIDTH;
    background_02.vpos = PLAYER_BASE_Y;

    bobcat_enableSprite(background_03);
    background_03.data = level2_side;
    background_03.hpos = 0;
    background_03.vpos = 102+PLAYER_BASE_Y;

    bobcat_enableSprite(background_04);
    bobcat_flipH(background_04);
    background_04.data = level2_side;
    background_04.hpos = SCREEN_WIDTH;
    background_04.vpos = 102+PLAYER_BASE_Y;

    bobcat_enableSprite(bridge_01);
    bridge_01.data = level2_bridge;
    bridge_01.hpos = 0;
    bridge_01.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(bridge_02);
    bridge_02.data = level2_bridge;
    bridge_02.hpos = 32;
    bridge_02.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(bridge_03);
    bridge_03.data = level2_bridge;
    bridge_03.hpos = 64;
    bridge_03.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(bridge_04);
    bridge_04.data = level2_bridge;
    bridge_04.hpos = 96;
    bridge_04.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(bridge_05);
    bridge_05.data = level2_bridge;
    bridge_05.hpos = 128;
    bridge_05.vpos = PLAYER_BASE_Y-1;
    bridge_05.vsize = 0x100;
    bridge_05.hsize = 0x100;

    bobcat_disableSprite(emote);
    emote.hpos = HIDDEN_X;
    emote.vpos = PLAYER_BASE_Y-30;
    

    //bobcat_enableSprite(player);

    //bobcat_disableSprite(web1);
    //bobcat_disableSprite(web2);
    //bobcat_disableSprite(web3);

    //bobcat_disableSprite(spider);

    bobcat_disableSprite(path);
    path.hsize = 0; //reset path
    path.hpos = 

    levelStep = 0;

    parallaxFX = 0;

    spider_init();
    webs_init();
}

void level2(){
    unsigned char work_pal[32];

    level2_init();

    

    ////////
    /// 0 - place player  
    //to be on par with level1 when debugging on level2 
    player_swichState(STATE_RUN);
    player_animSpeed += 5;

    timer = 0;
    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        if (player.hpos < SCREEN_WIDTH/2){
            player.hpos++; // move it to almost center of screen
            player_update();
            continue;;
        }
        timer++;
        if (timer == 1){
           //player.data = player_run_01; //rest 30 frames
        }else if (timer == 30){
           bobcat_enableSprite(emote);
           emote.data = emote_question_01;  //wait, what ?
           emote.hpos = player.hpos+5;
           bobcat_flipH(player);  //no more dragon ?
        }
        else if (timer == 110){
           bobcat_unflipH(player); //so what ?
           break;
        }
    }

    ///////
    /// 1 - spider entry
    ///////
    bobcat_disableSprite(emote);
    spider_propertiers.state  = SPIDER_STATE_ENTRY;
    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());
        
        spider_update(); 

        timer++;
        if (timer & 4)
        {
            spider.vpos++;
            if (spider.vpos == 0)   break;
        }
    }


    ///////
    /// 2 - bridge collapse
    ///////
    spider_propertiers.state  = SPIDER_STATE_IDLE;
    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());  

        spider_update(); 

        timer++;
        if (timer > 10)     bridge_02.vpos++;
        if (timer > 20)     bridge_04.vpos++;
        if (timer > 30)     bridge_01.vpos++;
        if (timer > 40)     bridge_05.vpos++;
        if (timer > 50)     bridge_03.vpos++;

        //check last bridge to start
        if (bridge_03.vpos > SCREEN_HEIGHT) break;
    }   

    ///////
    /// 3 - fall down
    ///////
    player_swichState(STATE_FALL);

    bobcat_disableSprite(bridge_01);
    bobcat_disableSprite(bridge_02);
    bobcat_disableSprite(bridge_03);
    bobcat_disableSprite(bridge_04);
    bobcat_disableSprite(bridge_05);

    bobcat_enableSprite(foreground_01);
    foreground_01.sprctl0 = background_01.sprctl0;
    foreground_01.hpos = background_01.hpos;
    foreground_01.vpos = background_01.vpos;
    foreground_01.data = background_01.data;

    bobcat_enableSprite(foreground_02);
    foreground_02.sprctl0 = background_02.sprctl0;
    foreground_02.hpos = background_02.hpos;
    foreground_02.vpos = background_02.vpos;
    foreground_02.data = background_02.data;

    bobcat_enableSprite(foreground_03);
    foreground_03.sprctl0 = background_03.sprctl0;
    foreground_03.hpos = background_03.hpos;
    foreground_03.vpos = background_03.vpos;
    foreground_03.data = background_03.data;

    bobcat_enableSprite(foreground_04);
    foreground_04.sprctl0 = background_04.sprctl0;
    foreground_04.hpos = background_04.hpos;
    foreground_04.vpos = background_04.vpos;
    foreground_04.data = background_04.data;

    background_01.data = level2_bg;
    background_02.data = level2_bg;
    background_03.data = level2_bg;
    background_04.data = level2_bg;

    

    bobcat_enableSprite(path);
    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        path.hsize+=10;
        if (path.hsize >= MAX_PATH_WIDTH)
        {
           path.hsize = MAX_PATH_WIDTH;
           break;
        }
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());  

        timer++;
        level2_update();

        

        //chase player, movement 2 times slower than player
        if (spider_propertiers.state != SPIDER_STATE_CHASE)
        {
            if ((spider.hpos - player.hpos) > 16)
                spider_propertiers.state = SPIDER_STATE_CHASE;
            else if ( 16 < (player.hpos -  spider.hpos))
                spider_propertiers.state = SPIDER_STATE_CHASE;
        }
        else if ((timer>>1)&1)
        {
            if (spider.hpos > player.hpos)
                spider.hpos--;
            else if ( spider.hpos < player.hpos)
                spider.hpos++;
            else
                spider_propertiers.state = SPIDER_STATE_IDLE;
        }
        spider_update(); 

        //attack!
        if (timer == 71){
            webs_launch();
            timer=0;
        }
        webs_update();

        if ( level2_checkCollide() ) {
            level2_death();
            isGameOver = 1;
            return;
        }


        // Joypad Actions------------------------------------------
        bobcat_joyupdate();
        if (joyChange & BUTTON_INNER)
        {
           
        }

        if (joyChange & BUTTON_OUTER)
        {
           
        }
        
        //move up
        if(joyCurrent & JOYPAD_UP){
           
        }
        
        //move left
        if(joyCurrent & JOYPAD_LEFT){
           bobcat_flipH(player);
           if (player.hpos > 16)
            player.hpos--;

        }
        
        //move down
        if(joyCurrent & JOYPAD_DOWN){
            
        }
        
        //move right
        if(joyCurrent & JOYPAD_RIGHT){
           bobcat_unflipH(player);
            if (player.hpos < (SCREEN_WIDTH-17))
                player.hpos++;
        }
    }   
   


    ///////
    /// 4 - move to right...
    ///////
    
    bobcat_unflipH(player); //go right
    path.hsize = MAX_PATH_WIDTH;
    spider_propertiers.state = SPIDER_STATE_IDLE;

    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());  

        timer++;
        level2_update(); 

        spider_update(); 
        webs_update();
        
        //move right
        player.hpos++;
        if (player.hpos == (SCREEN_WIDTH-17))   break;
    }  


    ///////
    /// 5 - ...to reach exit
    ///////
    
    bobcat_enableSprite(exit); 
    bobcat_flipH(exit);
    exit.hpos = background_02.hpos;
    exit.vpos = SCREEN_HEIGHT;
    exit.data = level2_exit;
    exit.hsize = SCALE_x1;
    exit.vsize = SCALE_x1;

    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());  

        timer++;
        level2_update(); 

        spider_update(); 
        webs_update();
        
        exit.vpos--;
        //jump door
        if (exit.vpos< (SCREEN_HEIGHT-30)){
            player.hpos++;
            if (player.hpos == SCREEN_WIDTH+17)   break;
        }
    }  

    
    ///////
    /// 6 - ...bye bye door
    ///////
    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());  

        timer++;
        level2_update(); 

        spider_update(); 
        webs_update();
        
        path.vpos--;
        exit.vpos--;
        if (exit.vpos< -50) break;
    }  

    ///////
    /// 7 - spider abandon
    ///////
    bobcat_disableSprite(path);

    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());  

        timer++;
        level2_update(); 

        spider_update(); 
        webs_update();
        
        spider.vpos--;
        if (spider.vpos< -40) break;
    }  

    ///////
    /// 8 - fadetoblack
    ///////
    for(timer = 0; timer < 32; timer++)
    {
        work_pal[timer] = src_gfx_game_sheet_pal[timer];
    }

    timer = 0;
    levelStep++;
    while(1){
        unsigned char colorID;
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());  


        tgi_setpalette(work_pal); 

        level2_update(); 

        timer++;
        for(colorID = 0; colorID < 16; colorID++)
        {
            if (  work_pal[colorID] > 0)                work_pal[colorID] -= 0x1;
            if ( (work_pal[colorID+16] &0xF) > 0)       work_pal[colorID+16] -= 0x1;
            if ( (work_pal[colorID+16] &0xF0) > 0)      work_pal[colorID+16] -= 0x10;
        }

        //we need F update to fade from white to black
        if (timer >= 0x20)  break;
    } 

    //switch to empty black screen
    bobcat_clearScreen();
    tgi_updatedisplay(); 
    while(tgi_busy());  
    tgi_setpalette(src_gfx_game_sheet_pal); 
}
