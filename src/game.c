#include "hungry_monsters.h"


#define WALL_WIDTH      160
#define SCROLL_SPEED    3
#define SPAWN_DELAY     200




#include "gfx/game_sheet.pal"


extern unsigned char hud_path[];




//need this struct to point to the seperate channels of music
typedef struct {
    unsigned char *music0;
    unsigned char *music1;
    unsigned char *music2;
    unsigned char *music3;
} song_t;

extern song_t musicptr; //pull in our externally referenced music data.


//---------------------Sprites
SCB_REHV  path = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    LAST_SPRITE,   
    hud_path,
    5       , 1, 
    0x0000  , SCALE_x1
};


SCB_REHV  monster = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &path,   
    NO_DATA,
    HIDDEN_X, 0,
    SCALE_x1, SCALE_x1
};
struct sprite_properties monster_properties;


SCB_REHV  enemy3 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &monster,   
    NO_DATA,
    HIDDEN_X, 0,
    SCALE_x1, SCALE_x1
};
struct sprite_properties enemy3_properties;


SCB_REHV enemy2 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &enemy3,   
    NO_DATA,
    HIDDEN_X, 20,
    SCALE_x1, SCALE_x1
};
struct sprite_properties enemy2_properties;

SCB_REHV  enemy1 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &enemy2,   
    NO_DATA, 
    HIDDEN_X, 0,
    SCALE_x1, SCALE_x1
};
struct sprite_properties enemy1_properties;


SCB_REHV  player = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &enemy1,
    NO_DATA,
    60      , 0,
    SCALE_x1, SCALE_x1
};
struct sprite_properties player_properties;

SCB_REHV  emote = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &player,
    emote_exclam_01,
    60      , 0,
    SCALE_x1, SCALE_x1
};

//shoot could be below player if he jumps on its last few right pixel
SCB_REHV  shoot = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &emote,   
    NO_DATA,
    HIDDEN_X, 0,
    SCALE_x1, SCALE_x1
};
struct sprite_properties shoot_properties;

SCB_REHV  foreground_04 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &shoot,
    NO_DATA,
    WALL_WIDTH  , 0, 
    SCALE_x1    , SCALE_x1
};

SCB_REHV  foreground_03 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &foreground_04,
    NO_DATA, 
    WALL_WIDTH  , 0,
    SCALE_x1    , SCALE_x1
};

SCB_REHV  foreground_02 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &foreground_03,
    NO_DATA,
    0       , 0,
    SCALE_x1, SCALE_x1
};


SCB_REHV  foreground_01 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &foreground_02,
    NO_DATA,
    0       , 0,
    SCALE_x1, SCALE_x1
};
SCB_REHV  background_04 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &foreground_01,
    NO_DATA,
    HIDDEN_X, 0,
    SCALE_x1, SCALE_x1
};

SCB_REHV  background_03 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &background_04,
    NO_DATA,
    HIDDEN_X, 0,
    SCALE_x1, SCALE_x1
};


SCB_REHV  background_02 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV | REUSEPAL,
    0x01,
    &background_03,
    NO_DATA,
    WALL_WIDTH , 0,
    SCALE_x1   , SCALE_x1
};


SCB_REHV_PAL  background_01 = {
    BPP_4 | TYPE_NONCOLL | SKIP,
    REHV,
    0x01,
    &background_02,
    NO_DATA, //sprite data
    0       , 0, //position x,y
    SCALE_x1, SCALE_x1,
    STANDARD_PENS
};

int timer; //generic timer, used for every timed thing (animation, pause, ..)

unsigned char scrollCounter; //counter before new scroll
unsigned char spawnTimer; //how many times before new span
unsigned char isGameOver;
unsigned char levelStep;


void sprites_init(){
    bobcat_resetSprite(background_01);
    bobcat_resetSprite(background_02);
    bobcat_resetSprite(background_03);
    bobcat_resetSprite(background_04);
    
    bobcat_resetSprite(foreground_01);
    bobcat_resetSprite(foreground_02);
    bobcat_resetSprite(foreground_03);
    bobcat_resetSprite(foreground_04);
    
    bobcat_resetSprite(shoot);
    bobcat_resetSprite(emote);
    bobcat_resetSprite(player);
    
    bobcat_resetSprite(enemy1);
    bobcat_resetSprite(enemy2);
    bobcat_resetSprite(enemy3);
    
    bobcat_resetSprite(monster);

    bobcat_resetSprite(path);
    path.data = hud_path;
    path.hsize = 0;
    path.vpos = 1;
    path.hpos = 5;
}

void game( ){	
    //play each channel of our song
    //lynx_snd_play ( 0 , musicptr.music0);
    //lynx_snd_play ( 1 , musicptr.music1);        
    //lynx_snd_play ( 2 , musicptr.music2);
    //lynx_snd_play ( 3 , musicptr.music3);
    
    //lynx_snd_continue();
    

    sprites_init();

    //force color 0 black
    src_gfx_game_sheet_pal[0] = 0;
    src_gfx_game_sheet_pal[16] = 0;
    tgi_setpalette(src_gfx_game_sheet_pal); 

    
    isGameOver = 0;
    player_init();

    ///// GO
    lynx_load(DIR_LEVEL1);
    level1();

    if (isGameOver == 0){
        lynx_load(DIR_LEVEL2);
        level2();
    }
    if (isGameOver == 0){
        lynx_load(DIR_LEVEL3);
        level3();
    }

    if (isGameOver == 0){
        lynx_load(DIR_LEVEL4);
        level4();
    }

    lynx_load(DIR_GAMEOVER);
    gameover();

    //clean
    //lynx_snd_stop();

    
    bobcat_clearScreen();
    tgi_updatedisplay(); 
    while(tgi_busy());  
}
