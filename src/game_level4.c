#include "hungry_monsters.h"


#pragma bss-name    ("LEVEL4_BSS")
#pragma code-name   ("LEVEL4_CODE")
#pragma data-name   ("LEVEL4_DATA")
#pragma rodata-name ("LEVEL4_RODATA")

void level4(){
    
    tgi_setpalette(tgi_getdefpalette());
	bobcat_setTextBgColor(0);   //transparent
	bobcat_setTextColor(0xF);   

    timer =  0;
    while (1)
    {   
        bobcat_clearScreen();
        tgi_outtextxy(5,10, "THAT'S ALL FOR NOW");
        tgi_outtextxy(5,20, "RETROGAMEJAM");
        tgi_outtextxy(4*5,30, "11/APR/2022 06:12");
        tgi_outtextxy(5,50, "THANKS TO");
        tgi_outtextxy(15,60, "FADEST ");
        tgi_outtextxy(15,70, "LORDKRAKEN");
        tgi_outtextxy(15,80, "FEI ");

        tgi_updatedisplay(); 
        while(tgi_busy());
        
        bobcat_joyupdate();
        if (joyChange & BUTTON_INNER)  break;
        if (++timer == 400) break;
    }

    //TODO fadeIn


    //clean
    bobcat_clearScreen();
    tgi_sprite(&background_01);
    tgi_updatedisplay(); 
    while(tgi_busy());
}
