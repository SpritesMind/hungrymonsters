#include "hungry_monsters.h"

#pragma bss-name    ("LOGO_BSS")
#pragma code-name   ("LOGO_CODE")
#pragma data-name   ("LOGO_DATA")
#pragma rodata-name ("LOGO_RODATA")

extern unsigned char logo_sm[]; //sprite
#include "gfx/logo_sm.pal"


//TODO used global sprites list
SCB_REHV_PAL  logo_spr = {
    BPP_4 | TYPE_NONCOLL,
    REHV,
    0x01,
    LAST_SPRITE,
    logo_sm, //sprite data
    30      , 30, 
    SCALE_x1, SCALE_x1,
    STANDARD_PENS 
};

void logo(){
    tgi_setpalette(src_gfx_logo_sm_pal);
    timer =  0;
    while (1)
    {
        
        bobcat_clearScreen();
        tgi_sprite(&logo_spr);

        tgi_updatedisplay(); 
        while(tgi_busy());
        
        bobcat_joyupdate();
        if (joyChange & BUTTON_INNER)  break;
        if (++timer == 200) break;
    }

    //TODO fadeIn


    //clean 
    bobcat_clearScreen();
    tgi_updatedisplay(); 
    while(tgi_busy());
}
