#include "hungry_monsters.h"


#pragma bss-name    ("LEVEL1_BSS")
#pragma code-name   ("LEVEL1_CODE")
#pragma data-name   ("LEVEL1_DATA")
#pragma rodata-name ("LEVEL1_RODATA")

#define WALL_WIDTH      SCREEN_WIDTH
#define SCROLL_SPEED    3
#define SPAWN_DELAY     200

#define PLAYER_BB_X         (60-5)
#define PLAYER_BB_Y         (PLAYER_BASE_Y - 24) //NOT WHILE JUMPING !!


////////////////////////
/// DRAGON stuff

#define dragon  monster
#define dragon_propertiers  monster_properties

extern unsigned char dragon_01[]; 
extern unsigned char dragon_02[]; 


//relative y to get a walking effect
//defined using
//https://www.daycounter.com/Calculators/Sine-Generator-Calculator2.phtml
//with N=40&A=60&Columns=8&Hex=1&B1=Submit
char dragon_moves[46] = {
    0x1e,0x23,0x27,0x2c,0x30,0x33,0x36,0x39,
    0x3b,0x3c,0x3c,0x3c,0x3b,0x39,0x36,0x33,
    0x30,0x2c,0x27,0x23,0x1e,0x19,0x15,0x10,
    0xc,0x9,0x6,0x3,0x1,0x0,0x0,0x0,
    0x0,0x0,0x0,0x0,0x0,0x0,
    0x1,0x3,0x6,0x9,0xc,0x10,0x15,0x19
};

unsigned char dragon_direction;
int dragon_baseX = -200;

void dragon_init(){
    dragon_baseX = -200;
    dragon_direction = 1;

    bobcat_enableSprite(dragon);
    dragon.data = dragon_01;
    dragon.hpos = dragon_baseX;
    dragon_propertiers.animCounter = 0;
}

void dragon_update(){
    dragon_direction++;
    if (dragon_direction == 92) dragon_direction = 0;
    dragon.vpos = 50 + 0x39 - dragon_moves[dragon_direction>>1]>>2;
    dragon.hpos = dragon_baseX - (dragon_moves[dragon_direction>>1]>>4);
    
    dragon_propertiers.animCounter++;
    if (dragon_propertiers.animCounter == 50)  dragon.data = dragon_02;
    if (dragon_propertiers.animCounter == 150){
        dragon.data = dragon_01;
        dragon_propertiers.animCounter = 0;
    }  
}

///////////////////////
//// LANCE
#define lance   shoot
#define lance_properties    shoot_properties //not used

extern unsigned char lance_01[]; 

void lance_init(){
    bobcat_disableSprite(lance);
    lance.data = lance_01;
    lance.hpos = HIDDEN_X;
    lance.vpos = PLAYER_BASE_Y-5;
}

void lance_update(){
    if (lance.hpos == HIDDEN_X) return;

    lance.hpos-=2;
    if (lance.hpos <= -40){
        bobcat_disableSprite(lance);
        lance.hpos = HIDDEN_X;
    }  
}


///////////////////////
/// WARRIOR

#define warrior enemy1
#define warrior_properties enemy1_properties

#define ESTATE_ATTACK   0
#define ESTATE_SHOOT    1
#define ESTATE_GETAWAY  2

extern unsigned char warrior_01[]; 
extern unsigned char warrior_02[]; 
extern unsigned char warrior_03[]; 
extern unsigned char warrior_04[]; 
extern unsigned char warrior_05[]; 
extern unsigned char warrior_06[]; 

void warrior_init(){
    bobcat_disableSprite(warrior);
    warrior.data = warrior_01;
    warrior.hpos = HIDDEN_X;
    warrior.vpos = PLAYER_BASE_Y;
    
    warrior_properties.animCounter = 0;
    warrior_properties.frame = 0;
    warrior_properties.state = NO_STATE;
}
void warrior_update(){

    warrior_properties.animCounter++;
    if (warrior_properties.animCounter == 5){
        warrior_properties.frame++;
        warrior_properties.frame %= 6; //6 frames
        switch (warrior_properties.frame)
        {
            case 1:
                warrior.data = warrior_02;
                break;
            case 2:
                warrior.data = warrior_03;
                break;
            case 3:
                warrior.data = warrior_04;
                break;
            case 4:
                warrior.data = warrior_05;
                break;
            case 5:
                warrior.data = warrior_06;
                break;
            
            default:
                warrior.data = warrior_01;
                break;
        }
        warrior_properties.animCounter = 0;
    }

    if (warrior.hpos == HIDDEN_X) return;

    switch(warrior_properties.state){
        case ESTATE_ATTACK:
            warrior.hpos--;
            if (warrior.hpos == 120){
                warrior_properties.state = ESTATE_GETAWAY;
                bobcat_flipH(warrior);

                bobcat_enableSprite(lance);
                lance.hpos = 120;
            }
            break;
        case ESTATE_GETAWAY:
            warrior.hpos++;
            if (warrior.hpos == 160){
                bobcat_disableSprite(warrior);
                bobcat_unflipH(warrior);
                warrior.hpos = HIDDEN_X;
            }
            break;
    }
}


///////////////////////
//// BAT

#define bat     enemy2
#define bat_properties     enemy2_properties

#define BSTATE_IDLE   0
#define BSTATE_ATTACK 1
#define BSTATE_FLY    2

extern unsigned char bat_idle_01[]; 
extern unsigned char bat_idle_02[]; 
extern unsigned char bat_idle_03[]; 
extern unsigned char bat_attack_01[]; 
extern unsigned char bat_attack_02[];

void bat_init(){
    bobcat_disableSprite(bat);
    bat.hpos = HIDDEN_X;
    bat.vpos = 20;
    bat.data = bat_idle_01;

    bat_properties.animCounter = 0;
    bat_properties.frame = 0;
    bat_properties.state = NO_STATE;
}



void bat_update(){
    if (bat.hpos == HIDDEN_X) return;

    if (bat_properties.state == BSTATE_IDLE){
        if (bat.hpos <= 120){
            bat_properties.state = BSTATE_ATTACK;
            bat.data = bat_attack_01;
        }
    }
    else if (bat_properties.state == BSTATE_ATTACK){
        bat.vpos+=2;
        if (bat.vpos > PLAYER_BASE_Y-10){
            bat_properties.state = BSTATE_FLY;
            //bat.data = bat_attack_01;
        }
    }
    else if (bat_properties.state == BSTATE_FLY){
        if (bat.hpos <= -20) {
            bobcat_disableSprite(bat);
            bat.hpos = HIDDEN_X;
        } 

        //see level1_update, since hpos is based on scroll
    }


    bat_properties.animCounter++;
    if (bat_properties.animCounter == 5){
        bat_properties.frame++;
        switch (bat_properties.state)
        {
            case BSTATE_IDLE:
                bat_properties.frame %= 3;
                switch (bat_properties.frame)
                {
                    case 1:
                        bat.data = bat_idle_02;
                        break;
                    case 2:
                        bat.data = bat_idle_03;
                        break;
    
                    default:
                        bat.data = bat_idle_01;
                        break;
                }
                break;
            case BSTATE_ATTACK:
                bat_properties.frame = 0;
                //no anim
                break;
            case BSTATE_FLY:
                bat_properties.frame %= 3;
                switch (bat_properties.frame)
                {
                    case 1:
                        bat.data = bat_attack_02;
                        break;
                    case 2:
                        bat.data = bat_attack_02;
                        break;
                    default:
                        bat.data = bat_attack_01;
                        break;
                }
                break;
        }

        bat_properties.animCounter = 0;
    }
}


///////////////////////
//// ROCK
#define rock    enemy3
#define rock_properties    enemy3_properties //not used

extern unsigned char rock_01[];

void rock_init( ){
    bobcat_disableSprite(rock);
    rock.hpos = HIDDEN_X;
    rock.vpos = PLAYER_BASE_Y-8;
    rock.data = rock_01;
    
    //rock
    bobcat_disableSprite(enemy3);
    rock.hpos = HIDDEN_X;
    rock.vpos = PLAYER_BASE_Y-8;
}
void rock_update(){
    if (rock.hpos <= -40) {
        bobcat_disableSprite(rock);
        rock.hpos = HIDDEN_X;
    } 

    //see level1_update, since hpos is based on scroll
}


////////////////////////////////

extern unsigned char level1_ceil[];
extern unsigned char level1_floor[];
extern unsigned char level1_chain[];
extern unsigned char level1_bones[];

//which enemy spawn
char enemySpawns[15] = {
    0,1,2,0,0,1,2,1,0,2,1,2,1,1,2
};
unsigned char spawnIndex;



unsigned char level1_enemy_spawn(){
    if (spawnIndex >= 15)
    {
        path.hsize = MAX_PATH_WIDTH;
        return 1;
    }   

    switch(enemySpawns[spawnIndex])
    {
        case 2:
            if (warrior.hpos != HIDDEN_X){
                spawnTimer = 1;
                return 0;
            } 

            bobcat_enableSprite(warrior);
            warrior.hpos = SCREEN_WIDTH + 10;
            warrior_properties.state = ESTATE_ATTACK;
            break;
        case 1:
            if (bat.hpos != HIDDEN_X) {
                spawnTimer = 1;
                return 0;
            } 

            bobcat_enableSprite(bat);
            bat.hpos = SCREEN_WIDTH + 10;
            bat.vpos = 20;
            bat_properties.state = BSTATE_IDLE;
            break;
        case 0:
        default:
            if (rock.hpos != HIDDEN_X){
                spawnTimer = 1;
                return 0;
            } 

            bobcat_enableSprite(rock);
            rock.hpos = SCREEN_WIDTH + 10;
            break;
    }

    //update path only if we spawn an enemyz
    //path is 160-10=> 150 pixels with
    //we have 14 enemies so, each update is 10pixel
    path.hsize = ((spawnIndex*10) << 8); 

    spawnIndex++;
    spawnTimer = SPAWN_DELAY+rand()%10;

    return 0;
}


unsigned char level1_checkCollide(){

    //for a horizontal control of 2 sprites A and B, we check
    // Aleft < Bright
    // Aright > Bleft

    ///////////////
    //rock : -S012008 -a000000
    if ( (rock.hpos < (PLAYER_BB_X + PLAYER_BB_WIDTH)) &&
         (rock.hpos > (PLAYER_BB_X - 12)) ) // (rock.hpos+ 12) > (PLAYER_BB_X)
    {
        //X collide OK
        //let's check Y collide
        if (player_properties.state != STATE_JUMP){
            return 1;
        }

        if ( (player.vpos -4) >= rock.vpos ){
            return 1;
        }
    }

    
    ///////////////
    //lance : -S028005 -a014003
    if ( (lance.hpos < (PLAYER_BB_X + PLAYER_BB_WIDTH + 14)) &&
         (lance.hpos > (PLAYER_BB_X - 8)) ) // not full lance ;)
    {
        //X collide OK
        //let's check Y collide
        if (player_properties.state != STATE_JUMP){
            return 1;
        }

        if ( (player.vpos -4) >= lance.vpos ){
            return 1;
        }
    }

    ///////////////
    //bat :-S020012 -a007008
    if ( (bat.hpos < (PLAYER_BB_X + PLAYER_BB_WIDTH + 7)) &&
         (bat.hpos > (PLAYER_BB_X - 7)) ) // not full width ;)
    {
        //X collide OK
        //let's check Y collide
        if (player_properties.state != STATE_JUMP){
            return 1;
        }

        if ( (player.vpos -4) >= bat.vpos ){
            return 1;
        }
    }

    return 0;
}




void level1_init(){

    scrollCounter = SCROLL_SPEED; 
    spawnTimer=1;
    spawnIndex=0;
    levelStep=0;

    //bones
    bobcat_enableSprite(background_01);
    background_01.data = level1_bones;
    background_01.hpos = SCREEN_WIDTH - rand();
    background_01.vpos = 6*8;

    //chains 1/2
    bobcat_enableSprite(background_02);
    background_02.data = level1_chain;
    background_02.hpos = SCREEN_WIDTH - rand();
    background_02.vpos = 2*8;

    //chain 2/2
    bobcat_enableSprite(background_03);
    background_03.data = level1_chain;
    background_03.hpos = 2*SCREEN_WIDTH - rand();
    background_03.vpos = 2*8;

    bobcat_disableSprite(background_04);

    //ceil 1/2
    bobcat_enableSprite(foreground_01);
    foreground_01.data = level1_ceil;
    foreground_01.hpos = 0;
    foreground_01.vpos = 0;
    //ceil 2/2
    bobcat_enableSprite(foreground_02);
    foreground_02.data = level1_ceil;
    foreground_02.hpos = WALL_WIDTH;
    foreground_02.vpos = 0;
    //floor 1/2
    bobcat_enableSprite(foreground_03);
    foreground_03.data = level1_floor;
    foreground_03.hpos = 0;
    foreground_03.vpos = 8*8;
    //floor 2/2
    bobcat_enableSprite(foreground_04);
    foreground_04.data = level1_floor;
    foreground_04.hpos = WALL_WIDTH;
    foreground_04.vpos = 8*8;

    
    //emote
    bobcat_disableSprite(emote);
    emote.hpos = HIDDEN_X;
    emote.vpos = PLAYER_BASE_Y-30; //y when it will be enabled


    dragon_init();

    warrior_init();
    lance_init();

    bat_init();
    rock_init();


    bobcat_disableSprite(path);
    path.hsize = 0;
}

void level1_update(){
    scrollCounter--;
    if (scrollCounter == 0)
    {
        scrollCounter = SCROLL_SPEED;

        //bones
        background_01.hpos-=2;
        if (background_01.hpos <= -32) {
            background_01.hpos = WALL_WIDTH + (rand() & 0x7F);
            background_01.vpos = 6*8 - (rand()&0x0F);

            //hide it
            if (levelStep >= 6) background_01.vpos = SCREEN_HEIGHT;
        }   
        //chains
        background_02.hpos-=2;
        if (background_02.hpos <= -8){
            background_02.hpos = WALL_WIDTH + (rand() & 0x7F);
            background_02.vpos = 2*8 - (rand()&0x1F);
            while ( abs(background_01.hpos - background_02.hpos) < 50 )   
                background_02.hpos = WALL_WIDTH + (rand() & 0x7F);

            if (levelStep >= 6) bobcat_disableSprite(background_02);
        }    

        background_03.hpos-=2;
        if (background_03.hpos <= -8){
            background_03.hpos = WALL_WIDTH + (rand() & 0x7F);
            background_03.vpos = 2*8 - (rand()&0x1F);
            while ( abs(background_01.hpos - background_03.hpos) < 50 )   
                background_03.hpos = WALL_WIDTH + (rand() & 0x7F);

            if (levelStep >= 6) bobcat_disableSprite(background_03);
        }    

        //ceil 1
        foreground_01.hpos-=2;
        if (foreground_01.hpos <= -WALL_WIDTH)    foreground_01.hpos = WALL_WIDTH;
        //floor 1
        foreground_02.hpos-=2;
        if (foreground_02.hpos <= -WALL_WIDTH)    foreground_02.hpos = WALL_WIDTH;
         //ceil 2
        foreground_03.hpos-=2;
        if (foreground_03.hpos <= -WALL_WIDTH)    foreground_03.hpos = WALL_WIDTH;
        //floor 2
        foreground_04.hpos-=2;
        if (foreground_04.hpos <= -WALL_WIDTH)    foreground_04.hpos = WALL_WIDTH;


        if (levelStep ==6)
        {
            //move bridge sprites
            monster.hpos -= 2;
            shoot.hpos -= 2;
            enemy1.hpos -= 2;
            enemy2.hpos -= 2;
            enemy3.hpos -= 2;
        }
        else
        {
            if (rock.hpos != HIDDEN_X) rock.hpos-=2;

            if (bat.hpos != HIDDEN_X) {
                bat.hpos-=2;
                if (bat_properties.state == BSTATE_FLY) 
                bat.hpos-=2; //faster
            }
        }
    }
}



void level1_death( ){
    //make everything at the right place
    dragon.data = dragon_01; //keep mouth open, meat is coming
    timer = 0;
    while(1)
    {
        bobcat_clearScreen();

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        level1_update(); //we still scroll

        warrior_update();
        lance_update();
        bat_update();
        rock_update();

        player_update(); 
        
        if ( dragon.vpos < 37 )   dragon.vpos++;
        //finish jump first
        if ( (player_properties.state != STATE_JUMP) && ( dragon.vpos == 37 ))  break; 

        timer++;
        if (timer & 8) 
            bobcat_disableSprite(player);
        else
            bobcat_enableSprite(player);
    }

    //bon appetit
    timer = 0; 
    while(1)
    {
        bobcat_clearScreen();

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        level1_update(); //we still scroll

        warrior_update();
        lance_update();
        bat_update();
        rock_update();

        player_update(); 

        timer++;
        if (timer == 40){
            player_animSpeed++;
        }   
        if (timer == 60){
            player_animSpeed++;
        }   
        if (timer == 80){
            player_animSpeed++;
        }   
        if (timer == 100){
            player_animSpeed++;
        }   
        if (timer == 120){
            player_animSpeed++;
        }   

       
        if (((timer&3) == 3 ) && (player.hpos >= -16)){
            player.hpos--;
            if (player.hpos == -16){
                dragon.data = dragon_02; //now close it and glups!
                break;
            }     
        }   

        if (timer & 8) 
            bobcat_disableSprite(player);
        else
            bobcat_enableSprite(player);
    }

    //dragon continues to look for meat
    timer = 0;
    while(1)
    {
        bobcat_clearScreen();

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        level1_update(); //we still scroll
       
        if (dragon.vpos > 20)   dragon.vpos--;

        timer++;
        if (timer == 40) break;
    }
}


extern unsigned char level2_side[]; 
extern unsigned char level2_bridge[]; 
void level1(){
    level1_init();

    player_swichState(STATE_RUN);
    player_animSpeed = 7; //make it walk, not run ;)

    ///////
    /// 0 - player enters screen
    ///////
    timer = 0;
    while(1)
    {
        bobcat_clearScreen();

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        player_update(); 
        timer++;
        if (timer == 2){
            timer=0;
            player.hpos++;
            if (player.hpos == 60)  break; //stay there !
        }
    }

    ///////
    /// 1 - player senses something
    ///////
    //player.data = player_run_01;
    timer = 0;
    levelStep++;
    while(1)
    {
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        timer++;
        if (timer == 30){
            bobcat_enableSprite(emote);
            emote.data = emote_question_01;
            emote.hpos = player.hpos+5;
        }
        if (timer == 60){
            bobcat_flipH(player);
        }
        if (timer == 90){
            bobcat_unflipH(player); 
        }
        if (timer == 120){
            player_swichState(STATE_AFRAID);
            bobcat_flipH(player); 
            emote.data = emote_exclam_01;
            break;
        }
    }

    ///////
    /// 2 - dragon enters screen
    ///////
    levelStep++;
    while(1){
        bobcat_clearScreen();

        dragon_baseX++;
        if (dragon_baseX == -20)   break;

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        //level1_update();  
        dragon_update(); 
        player_update(); 
    }


    bobcat_disableSprite(emote);
    emote.hpos = HIDDEN_X; //in case we reenable it 

    
    bobcat_enableSprite(shoot);
    bobcat_enableSprite(enemy1);
    bobcat_enableSprite(enemy2);
    bobcat_enableSprite(enemy3);
    bobcat_enableSprite(path);


    player_swichState(STATE_RUN);
    level1_enemy_spawn();
   
   
    ///////
    /// 3 - player runs for his life
    ///////
    levelStep++;
    while (1)
    {
        //update effect
        //tgi_setpalette(src_gfx_game_sheet_pal);

        //we need to clear transparent section
        bobcat_clearScreen(); 
        
        //draw our sprites for this VBL
        tgi_sprite(&background_01);
        
        //vsync and update everything
        tgi_updatedisplay(); 
        while(tgi_busy());
        
        if (--spawnTimer == 0)
        {
            if ( level1_enemy_spawn() ) 
                break; //end of level
        }

        level1_update();

        dragon_update();

        warrior_update();
        lance_update();

        bat_update();

        rock_update();

        player_update();

        if ( level1_checkCollide() ) {
            level1_death();
            isGameOver = 1;
            return;
        } 

        // Joypad Actions------------------------------------------
        bobcat_joyupdate();
        if (joyChange & BUTTON_INNER)
        {
            player_swichState(STATE_JUMP);
        }

        if (joyChange & BUTTON_OUTER)
        {
           
        }
        
        //move up
        if(joyCurrent & JOYPAD_UP){
           
        }
        
        //move left
        if(joyCurrent & JOYPAD_LEFT){
           
        }
        
        //move down
        if(joyCurrent & JOYPAD_DOWN){
            
        }
        
        //move right
        if(joyCurrent & JOYPAD_RIGHT){
            
        }
    };


    ///////
    /// 4 - you win, dragon abandons pursuit
    ///////
    levelStep++;
    while(1){
        dragon_baseX--;
        if (dragon_baseX == -200)   break;

        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        level1_update();  
        dragon_update(); 
        player_update(); 
    }


    ///////
    /// 5 - run up to ceil hides...
    ///////
    timer = 0;
    levelStep++;

    

    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   


        level1_update();
        player_update(); 
        //if (foreground_01.hpos == WALL_WIDTH) {
        //    foreground_01.data = level2_side;
        //    bobcat_disableSprite(foreground_03);
        if (foreground_02.hpos == WALL_WIDTH) {
            foreground_02.data = level2_side;
            foreground_02.vpos = PLAYER_BASE_Y;
            bobcat_disableSprite(foreground_04);

            //i can't disable this one :(
            if (background_01.hpos > WALL_WIDTH)    background_01.vpos = SCREEN_HEIGHT;
            if (background_02.hpos > WALL_WIDTH)    bobcat_disableSprite(background_02);
            if (background_03.hpos > WALL_WIDTH)    bobcat_disableSprite(background_03);
            if (background_04.hpos > WALL_WIDTH)    bobcat_disableSprite(background_04);
            break;
        }
    }
    
    
    ///////
    /// 6 - ..so we can set it to another bitmap
    /// slow down

    //reaffect
    bobcat_enableSprite(shoot);
    shoot.data = level2_bridge;
    shoot.hpos = WALL_WIDTH;
    shoot.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(enemy1);
    enemy1.data = level2_bridge;
    enemy1.hpos = WALL_WIDTH+32;
    enemy1.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(enemy2);
    enemy2.data = level2_bridge;
    enemy2.hpos = WALL_WIDTH+64;
    enemy2.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(enemy3);
    enemy3.data = level2_bridge;
    enemy3.hpos = WALL_WIDTH+96;
    enemy3.vpos = PLAYER_BASE_Y-1;
    bobcat_enableSprite(monster);
    monster.data = level2_bridge;
    monster.hpos = WALL_WIDTH+128;
    monster.vpos = PLAYER_BASE_Y-1;

    timer= 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        //TODO bridge
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        timer++;
        //slow down
        if (timer == 30)    player_animSpeed++;
        if (timer == 40)    player_animSpeed++;
        if (timer == 50)    player_animSpeed++;
        if (timer == 60)    player_animSpeed++;
        if (timer == 70)    player_animSpeed++;

        level1_update(); //we still scroll
        player_update(); 
        
        //if (foreground_01.hpos == 0) break;
        if (foreground_02.hpos == 0) break;
    }
}