#include "hungry_monsters.h"


#pragma bss-name    ("LEVEL3_BSS")
#pragma code-name   ("LEVEL3_CODE")
#pragma data-name   ("LEVEL3_DATA")
#pragma rodata-name ("LEVEL3_RODATA")

#define WALL_WIDTH      SCREEN_WIDTH
#define SCROLL_SPEED    3
#define SPAWN_DELAY     200



extern char src_gfx_game_sheet_pal[]; //see game_level1

////////////////////////////////

extern unsigned char level1_ceil[];
extern unsigned char level1_floor[];

extern unsigned char whip[];

/////////////
//CHEST
#define chest shoot
#define chest_properties shoot_properties
#define whip_spr enemy1

extern unsigned char chest_01[];
extern unsigned char chest_02[];
extern unsigned char chest_03[];
extern unsigned char chest_04[];
extern unsigned char chest_05[];

void chest_init(){
    bobcat_enableSprite(chest);
    chest.vpos = PLAYER_BASE_Y;
    chest.hpos = SCREEN_WIDTH + 20;
    chest.data = chest_01;

    chest_properties.animCounter = 0;
    chest_properties.frame = 0;
    chest_properties.state = NO_STATE;
}

#define CHEST_FRAME_DELAY   7
void chest_update()
{
    if (levelStep == 3)
    {
        if (chest_properties.animCounter >= 12*CHEST_FRAME_DELAY)   return;

        chest_properties.animCounter++;
        if (chest_properties.animCounter == CHEST_FRAME_DELAY)      chest.data = chest_02;
        if (chest_properties.animCounter == 2*CHEST_FRAME_DELAY)    chest.data = chest_03;
        if (chest_properties.animCounter == 3*CHEST_FRAME_DELAY)  chest.data = chest_04;
        if (chest_properties.animCounter == 4*CHEST_FRAME_DELAY)  chest.data = chest_05;
        if (chest_properties.animCounter == 5*CHEST_FRAME_DELAY)  chest.data = chest_04;
        if (chest_properties.animCounter == 6*CHEST_FRAME_DELAY)  chest.data = chest_03;
        if (chest_properties.animCounter == 7*CHEST_FRAME_DELAY)  chest.data = chest_02;
        if (chest_properties.animCounter == 8*CHEST_FRAME_DELAY)  chest.data = chest_01;
        if (chest_properties.animCounter == 9*CHEST_FRAME_DELAY)  chest.data = chest_02;
        if (chest_properties.animCounter == 10*CHEST_FRAME_DELAY)  chest.data = chest_03;
        if (chest_properties.animCounter == 11*CHEST_FRAME_DELAY)  chest.data = chest_04;
        if (chest_properties.animCounter == 12*CHEST_FRAME_DELAY){
            chest.data = chest_05;
            
            bobcat_enableSprite(whip_spr);
            whip_spr.hpos = chest.hpos;
            whip_spr.vpos = chest.vpos;
            whip_spr.data = whip;
        }  
    }
}



////////////////////////////

void level3_init(){
    scrollCounter = SCROLL_SPEED; 
    levelStep=0;

    //ceil 1/2
    bobcat_enableSprite(background_01);
    bobcat_unflipH(background_01);
    background_01.data = level1_ceil;
    background_01.hpos = 0;
    background_01.vpos = 0;
    //ceil 2/2
    bobcat_enableSprite(background_02);
    bobcat_unflipH(background_02);
    background_02.data = level1_ceil;
    background_02.hpos = WALL_WIDTH;
    background_02.vpos = 0;


    //floor 1/2
    bobcat_enableSprite(background_03);
    bobcat_unflipH(background_03);
    background_03.data = level1_floor;
    background_03.hpos = 0;
    background_03.vpos = 8*8;
    //floor 2/2
    bobcat_enableSprite(background_04);
    bobcat_unflipH(background_04);
    background_04.data = level1_floor;
    background_04.hpos = WALL_WIDTH;
    background_04.vpos = 8*8;

    bobcat_disableSprite(foreground_01);
    bobcat_disableSprite(foreground_02);
    bobcat_disableSprite(foreground_03);
    bobcat_disableSprite(foreground_04);

    
    //emote
    bobcat_disableSprite(emote);
    emote.hpos = HIDDEN_X;
    emote.vpos = PLAYER_BASE_Y-30; //y when it will be enabled

    bobcat_disableSprite(path);
    path.hsize = 0;
}

void level3_update(){
    scrollCounter--;
    if (scrollCounter == 0)
    {
        scrollCounter = SCROLL_SPEED;

        //ceil 1
        background_01.hpos-=2;
        if (background_01.hpos <= -WALL_WIDTH)    background_01.hpos = WALL_WIDTH;
        //floor 1
        background_02.hpos-=2;
        if (background_02.hpos <= -WALL_WIDTH)    background_02.hpos = WALL_WIDTH;
         //ceil 2
        background_03.hpos-=2;
        if (background_03.hpos <= -WALL_WIDTH)    background_03.hpos = WALL_WIDTH;
        //floor 2
        background_04.hpos-=2;
        if (background_04.hpos <= -WALL_WIDTH)    background_04.hpos = WALL_WIDTH;

        if (levelStep == 2) chest.hpos-=2;
        if (levelStep == 4) whip_spr.hpos-=2;
    }
}


void level3(){
    unsigned char work_pal[32];


    level3_init();

    chest_init();

    player_swichState(STATE_RUN);
    player_animSpeed = 7; //make it walk, not run ;)
    player.hpos = -16;

    ///////
    /// 0 - player enters screen
    ///////
    timer = 0;
    while(1)
    {
        bobcat_clearScreen();

        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());   

        //no scroll yet

        player_update(); 
        timer++;
        if (timer == 2){
            timer=0;
            player.hpos++;
            if (player.hpos == 60)  break; //stay there !
        }
    }

   
    ///////
    /// 1 - player walk
    ///////
    levelStep++;
    while (1)
    {
        //we need to clear transparent section
        bobcat_clearScreen(); 
        
        //draw our sprites for this VBL
        tgi_sprite(&background_01);
        
        //vsync and update everything
        tgi_updatedisplay(); 
        while(tgi_busy());
        
        timer++;
        if (timer == 120)   break;

        level3_update();
        player_update(); 
    };

    ///////
    /// 2 - chest
    ///////
    timer = 0;
    levelStep++;

    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());

        level3_update();
        player_update(); 
        if (chest.hpos < player.hpos+16)    break;
    }

    ///////
    /// 3 - parchemin
    ///////
    timer = 0;
    levelStep++;

    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());

        //level3_update();
        //player_update(); 
        chest_update();

        //TODO : bump
        timer++;
        if (timer==130)  break;
    }

    ///////
    /// 4 - get whip
    ///////
    timer = 0;
    levelStep++;

    bobcat_disableSprite(chest);

    while(1){
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());

        level3_update();
        player_update(); 

        if (player.hpos >= whip_spr.hpos)   break;
    }

    ///////
    /// 5 - fade to white
    ///////
    for(timer = 0; timer < 32; timer++)
    {
        work_pal[timer] = src_gfx_game_sheet_pal[timer];
    }

    bobcat_disableSprite(whip_spr);


    timer = 0;
    levelStep++;
    while(1){
        unsigned char colorID;
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());

        timer++;
        for(colorID = 0; colorID < 16; colorID++)
        {
            if (  work_pal[colorID] < 0xF)                work_pal[colorID] += 0x1;
            if ( (work_pal[colorID+16] &0xF) < 0xF)       work_pal[colorID+16] += 0x1;
            if ( (work_pal[colorID+16] &0xF0) < 0xF0)      work_pal[colorID+16] += 0x10;
        }
        
        tgi_setpalette(work_pal); 

        //we need F update to fade from white to black
        if (timer == 0x20)  break;
    }

    ///////
    /// 6 - wait a little
    ///////
    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        //tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());

        timer++;
        if (timer == 0x20)  break;
    }

    ///////
    /// 7 - fade to pal to discover new adventurer
    ///////
    /*
    player_swichState(STATE_INDY);

    timer = 0;
    levelStep++;
    while(1){
        unsigned char colorID;
        bobcat_clearScreen();
        tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());

        timer++;
        for(colorID = 0; colorID < 16; colorID++)
        {
            if (  work_pal[colorID] > src_gfx_game_sheet_pal[colorID])                      work_pal[colorID] -= 0x1;
            if ( (work_pal[colorID+16] &0xF) > (src_gfx_game_sheet_pal[colorID+16]&0xF) )       work_pal[colorID+16] -= 0x1;
            if ( (work_pal[colorID+16] &0xF0) > (src_gfx_game_sheet_pal[colorID+16]&0xF0) )      work_pal[colorID+16] -= 0x10;
        }
        
        tgi_setpalette(work_pal); 

        //we need F update to fade from white to black
        if (timer == 0x20)  break;

        player_update();
    }
    */
    ///////
    /// 8 - wait a little
    ///////
    timer = 0;
    levelStep++;
    while(1){
        bobcat_clearScreen();
        //tgi_sprite(&background_01);
        tgi_updatedisplay(); 
        while(tgi_busy());


        timer++;
        if (timer == 0x20)  break;
    }

    tgi_setpalette(src_gfx_game_sheet_pal); 

    bobcat_clearScreen();
    //tgi_sprite(&background_01);
    tgi_updatedisplay(); 
    while(tgi_busy());
}