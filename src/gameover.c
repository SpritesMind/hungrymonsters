#include "hungry_monsters.h"



#pragma bss-name    ("GAMEOVER_BSS")
#pragma code-name   ("GAMEOVER_CODE")
#pragma data-name   ("GAMEOVER_DATA")
#pragma rodata-name ("GAMEOVER_RODATA")

void gameover(){
    
    tgi_setpalette(tgi_getdefpalette());
	bobcat_setTextBgColor(0);   //transparent
	bobcat_setTextColor(0xF);   

    timer =  0;
    while (1)
    {   
        bobcat_clearScreen();
        tgi_outtextxy((SCREEN_WIDTH - 8*9)/2,(SCREEN_HEIGHT - 8)/2, "GAME OVER");

        tgi_updatedisplay(); 
        while(tgi_busy());
        
        bobcat_joyupdate();
        if (joyChange & BUTTON_INNER)  break;
        if (++timer == 200) break;
    }

    //TODO fadeIn


    //clean
    //nothing
}
