#include "hungry_monsters.h"


LNX_HEADER("Hungry Monsters", "SpritesMind");


extern void logo();
extern void title();
extern void game();

void main(void){	
	bobcat_initialize();

    lynx_load(DIR_LOGO);
    logo();

    while(1)
    {
        lynx_load(DIR_TITLE);
        title();

        game();
    };
}
