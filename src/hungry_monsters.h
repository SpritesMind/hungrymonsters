#ifndef _HUNGRYMONSTERS_H
#define _HUNGRYMONSTERS_H

#include <bobcat.h>

///DIR entries
#define DIR_MAIN    0 //we don't really care, since it is autoloaded at start
#define DIR_LOGO    DIR_MAIN+1
#define DIR_TITLE   DIR_LOGO+1
#define DIR_LEVEL1  DIR_TITLE+1
#define DIR_LEVEL2  DIR_LEVEL1+1
#define DIR_LEVEL3  DIR_LEVEL2+1
#define DIR_LEVEL4  DIR_LEVEL3+1
#define DIR_GAMEOVER  DIR_LEVEL4+1



////////////////
//X used to hide a sprite

extern int timer; //generic timer, used for every timed thing (animation, pause, ..)

extern unsigned char scrollCounter; //counter before new scroll
extern unsigned char spawnTimer; //how many times before new span
extern unsigned char isGameOver; //1 if gameover


#define NO_STATE    0xFF

struct sprite_properties{
    unsigned char animCounter;  //change frame when reach 0
    unsigned char frame;        //current frame
    
    unsigned char state;
};

///////////////////////
//  SPRITES LIST, by draw order

extern SCB_REHV_PAL  background_01;
extern SCB_REHV  background_02;
extern SCB_REHV  background_03;
extern SCB_REHV  background_04;

extern SCB_REHV  foreground_01;
extern SCB_REHV  foreground_02;
extern SCB_REHV  foreground_03;
extern SCB_REHV  foreground_04;


extern SCB_REHV shoot;
extern struct sprite_properties shoot_properties;

extern SCB_REHV  emote;
extern unsigned char emote_exclam_01[];
extern unsigned char emote_question_01[];


extern SCB_REHV  player;
extern struct sprite_properties player_properties;

extern SCB_REHV enemy1;
extern struct sprite_properties enemy1_properties;

extern SCB_REHV enemy2;
extern struct sprite_properties enemy2_properties;

extern SCB_REHV enemy3;
extern struct sprite_properties enemy3_properties;


extern SCB_REHV monster;
extern struct sprite_properties monster_properties;

extern SCB_REHV  path;  //lastone


////////////
/// LEVELS
#define MAX_PATH_WIDTH  ((SCREEN_WIDTH-(2*5)) << 8)


extern unsigned char levelStep;
extern void level1();
extern void level2();
extern void level3();
extern void level4();
extern void gameover();

/////////////
// PLAYER

#define PLAYER_BASE_Y   (63+32)
#define PLAYER_BB_WIDTH     (10)
#define PLAYER_BB_HEIGHT    (20)

#define STATE_AFRAID    0
#define STATE_RUN       1
#define STATE_JUMP      2
#define STATE_FALL      3


extern void player_swichState(int newState);
extern void player_init();
extern void player_update();

extern unsigned char player_animSpeed;

#endif //_HUNGRYMONSTERS_H
