
	.include "lynx.inc"
	.import         __STARTOFDIRECTORY__
	.import         __MAIN_START__, __SHARED_RAM__
	.import         __BANK0BLOCKSIZE__
	.import		__SHARED_RAM_SIZE__
	.import		__LOGO_CODE_SIZE__, __LOGO_DATA_SIZE__, __LOGO_RODATA_SIZE__
	.import		__LOGO_CODE_LOAD__
	.import		__TITLE_CODE_SIZE__, __TITLE_DATA_SIZE__, __TITLE_RODATA_SIZE__
	.import		__LEVEL1_CODE_SIZE__, __LEVEL1_DATA_SIZE__, __LEVEL1_RODATA_SIZE__
	.import		__LEVEL2_CODE_SIZE__, __LEVEL2_DATA_SIZE__, __LEVEL2_RODATA_SIZE__
	.import		__LEVEL3_CODE_SIZE__, __LEVEL3_DATA_SIZE__, __LEVEL3_RODATA_SIZE__
	.import		__LEVEL4_CODE_SIZE__, __LEVEL4_DATA_SIZE__, __LEVEL4_RODATA_SIZE__
	.import		__GAMEOVER_CODE_SIZE__, __GAMEOVER_DATA_SIZE__, __GAMEOVER_RODATA_SIZE__
	.import		__STARTUP_SIZE__, __LOWCODE_SIZE__, __ONCE_SIZE__, __CODE_SIZE__, __RODATA_SIZE__, __DATA_SIZE__
;; this is mandatory to load loader related cc65 function (load, seek...)
	.export         __DEFDIR__: absolute = 1

	.export _DIR_MAIN : absolute
	.export _DIR_LOGO : absolute
	.export _DIR_TITLE : absolute
	.export _DIR_LEVEL1 : absolute
	.export _DIR_LEVEL2 : absolute
	.export _DIR_LEVEL3 : absolute
	.export _DIR_LEVEL4 : absolute
	.export _DIR_GAMEOVER : absolute

.segment "DIRECTORY"


;;;;;;;;;;;;;;
;;; entry format
;; 
;;  see https://atarilynxdeveloper.wordpress.com/2014/09/10/programming-tutorial-part-18files/
;;
;;	byte	pageNumber
;;	word	offset
;;	byte	flags (?????)
;;  word	loadAddress
;;	word	length


__DIRECTORY_START__:

logoSize = __LOGO_CODE_SIZE__ + __LOGO_DATA_SIZE__ + __LOGO_RODATA_SIZE__
titleSize = __TITLE_CODE_SIZE__ + __TITLE_DATA_SIZE__ + __TITLE_RODATA_SIZE__
level1Size = __LEVEL1_CODE_SIZE__ + __LEVEL1_DATA_SIZE__ + __LEVEL1_RODATA_SIZE__
level2Size = __LEVEL2_CODE_SIZE__ + __LEVEL2_DATA_SIZE__ + __LEVEL2_RODATA_SIZE__
level3Size = __LEVEL3_CODE_SIZE__ + __LEVEL3_DATA_SIZE__ + __LEVEL3_RODATA_SIZE__
level4Size = __LEVEL4_CODE_SIZE__ + __LEVEL4_DATA_SIZE__ + __LEVEL4_RODATA_SIZE__
gameOverSize = __GAMEOVER_CODE_SIZE__ + __GAMEOVER_DATA_SIZE__ + __GAMEOVER_RODATA_SIZE__
mainSize = __STARTUP_SIZE__ + __LOWCODE_SIZE__ + __ONCE_SIZE__ + __CODE_SIZE__ + __RODATA_SIZE__ + __DATA_SIZE__

offLogo = __STARTOFDIRECTORY__ + (__DIRECTORY_END__ - __DIRECTORY_START__)
offTitle = offLogo + logoSize
offLevel1 = offTitle + titleSize
offLevel2 = offLevel1 + level1Size
offLevel3 = offLevel2 + level2Size
offLevel4 = offLevel3 + level3Size
offGameover = offLevel4 + level4Size
offMain = offGameover + gameOverSize


; Entry 0 - main executable, auto loaded
_DIR_MAIN=0
block0 = offMain / __BANK0BLOCKSIZE__
        .byte   <block0
        .word   offMain & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __MAIN_START__
        .word   mainSize

; Entry 1 - LOGO
_DIR_LOGO=1
block1 = offLogo / __BANK0BLOCKSIZE__
        .byte   <block1
        .word   offLogo & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __SHARED_RAM__
        .word   __SHARED_RAM_SIZE__

; Entry 2 - TITLE
_DIR_TITLE=2
block2 = offTitle / __BANK0BLOCKSIZE__
        .byte   <block2
        .word   offTitle & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __SHARED_RAM__
        .word   __SHARED_RAM_SIZE__

; Entry 3 - LEVEL 1
_DIR_LEVEL1=3
block3 = offLevel1 / __BANK0BLOCKSIZE__
        .byte   <block3
        .word   offLevel1 & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __SHARED_RAM__
        .word   __SHARED_RAM_SIZE__


; Entry 4 - LEVEL 2
_DIR_LEVEL2=4
block4 = offLevel2 / __BANK0BLOCKSIZE__
        .byte   <block4
        .word   offLevel2 & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __SHARED_RAM__
        .word   __SHARED_RAM_SIZE__

; Entry 5 - LEVEL 3
_DIR_LEVEL3=5
block5 = offLevel3 / __BANK0BLOCKSIZE__
        .byte   <block5
        .word   offLevel3 & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __SHARED_RAM__
        .word   __SHARED_RAM_SIZE__


; Entry 6 - LEVEL 3
_DIR_LEVEL4=6
block6 = offLevel4 / __BANK0BLOCKSIZE__
        .byte   <block6
        .word   offLevel4 & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __SHARED_RAM__
        .word   __SHARED_RAM_SIZE__

; Entry 7 - GAMEOVER
_DIR_GAMEOVER=7
block7 = offGameover / __BANK0BLOCKSIZE__
        .byte   <block7
        .word   offGameover & (__BANK0BLOCKSIZE__ - 1)
        .byte   $88
        .word   __SHARED_RAM__
        .word   __SHARED_RAM_SIZE__

__DIRECTORY_END__:


