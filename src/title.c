#include "hungry_monsters.h"


#pragma bss-name    ("TITLE_BSS")
#pragma code-name   ("TITLE_CODE")
#pragma data-name   ("TITLE_DATA")
#pragma rodata-name ("TITLE_RODATA")


#define TEXTCOLOR   15
extern unsigned char hungry[]; //sprite
extern unsigned char press_button[]; //sprite
extern unsigned char monster_head[]; //sprite

#include "gfx/title_screen.pal"

//TODO use global sprites list

 //chained sprites list
SCB_REHVS  title_button = {
    BPP_4 | TYPE_BACKGROUND,
    REHV | REUSEPAL,
    0x01,
    LAST_SPRITE,
    press_button, //sprite data
    22      , 84, 
    SCALE_x1, SCALE_x1
};

SCB_REHVS  title_head = {
    BPP_4 | TYPE_BACKGROUND,
    REHV | REUSEPAL,
    0x01,
    &title_button,
    monster_head, //sprite data
    0,       0, 
    SCALE_x1,SCALE_x1
};

SCB_REHV_PAL  title_logo = {
    BPP_4 | TYPE_BACKGROUND,
    REHV,
    0x01,
    &title_head,
    hungry, //sprite data
    80      , 8,
    SCALE_x1, SCALE_x1,
    STANDARD_PENS
};


void title(){
    int fade_delay = 0;
    int fade_direction = 0;

    //TODO fadeOut
       
    src_gfx_title_screen_pal[TEXTCOLOR] = 0xF;
    src_gfx_title_screen_pal[TEXTCOLOR+16] = 0xFF;
    tgi_setpalette(src_gfx_title_screen_pal);

    while (1)
    {
        bobcat_clearScreen();

        tgi_sprite(&title_logo);

        fade_delay++;
        if (fade_delay >= 5){
            if (fade_direction == 2){ 
                fade_direction = 0; 
                fade_delay = -100; //stay on full visibility some ms
            }
            else if (fade_direction == 0){ 

                src_gfx_title_screen_pal[TEXTCOLOR] -= 0x01;
                src_gfx_title_screen_pal[TEXTCOLOR+16] -= 0x011;
                fade_delay = 0;
                
                if(src_gfx_title_screen_pal[TEXTCOLOR] == 0x0)   fade_direction = 1; 
            }
            else //if(fade_direction == 1 && fade_delay >= 5)
            {
                src_gfx_title_screen_pal[TEXTCOLOR] += 0x01;
                src_gfx_title_screen_pal[TEXTCOLOR+16] += 0x011;
                fade_delay = 0;
        
                if(src_gfx_title_screen_pal[TEXTCOLOR] >= 0xF)   fade_direction = 2;  
            }
        }
        tgi_setpalette(src_gfx_title_screen_pal);

        tgi_updatedisplay(); 
        while(tgi_busy());
        
           
        bobcat_joyupdate();
        if (joyChange & BUTTON_INNER)  break;
    }


    //clean
    bobcat_clearScreen();
    tgi_updatedisplay(); 
    while(tgi_busy());

}